namespace PrisonerDilemma;

/// <summary>
/// Always defects
/// </summary>
class PrisonerDefect : IPrisoner
{
  public string Name { get; } = "Defector";

  public void Reset(DilemmaParameters dilemmaParameters) { }

  public PrisonerChoice GetChoice()
  {
    return PrisonerChoice.Defect;
  }

  public void ReceiveOtherChoice(PrisonerChoice otherChoice) { }
}